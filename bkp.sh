#!/bin/bash

# Script de Backup no S3 
# Desenvolvido Hernani Soares
# https://www.linkedin.com/in/soaresnetoh/
# Criado   em : 07/12/2020 - Por Hernani Soares - soaresnetoh@gmail.com
# Alterado em : 07/12/2020 - Por Hernani Soares - soaresnetoh@gmail.com

# Variáveis
mailOrigem='soaresnetoh@gmail.com'
mailDestino='soaresnetoh@gmail.com'

DATE=`date +%d%m%y%H%M`
proc=`ps aux | grep "aws s3" | grep -v grep | awk '{print $2}' | xargs | awk '{print $1}'`;

log='/home/usuario/treinamento/backup/logs/s3.txt'
dir_bkp='/home/usuario/treinamento/backup/bkps'


# Verifica se existe os arquivo com as pastas a serem feitas backup
if ! [ -f "./diretorios.txt" ] ; then
    echo "=========================================="
    echo "   O arquivo diretorios.txt não existe"
    echo "=========================================="
    echo "Crie e digite dentro dele o path completo"
    echo "dos diretorios a serem feito backup"
    echo "=========================================="
    exit 1

fi

# Checa Processos
if [ $proc ]
then
    echo "Backup em Andamento no AWS S3 " -`date +%d/%m/%Y-%H:%M:%S` >> $log;
    echo " " >> $log;
elif [ ! -d $dir_bkp ]
then
    echo "Diretorio "+$dir_bkp+" de backup não existe" -`date +%d/%m/%Y-%H:%M:%S` >> $log;
    echo " " >> $log;
else
    echo "Inicio de Backup "-`date +%d/%m/%Y-%H:%M:%S` >> $log;
fi

# Neste momento vc faz um backup local e depois envia este backup local para a Nuvem. Com isso, em caso de solicitação de restauração de algum arquivo, primeiramente voce busca no backup Local.

echo -e "Backup das pastas"

for line in $(cat diretorios.txt); 
    do 
    echo "$line";
    echo "Diretorio $line"
    #escolha qual variavel abaixo a usar no nome do arquivo compactado
    dirname=${line##*/} # pega somente o nome do diretorio
    pathdirname=`echo $line | sed 's/\//-/g'` # pega o path completo do diretorio e substitui a bassa pelo traço

    tar -czvf $dir_bkp/$pathdirname-`date +%Y%m%d`.tar.gz --absolute-names $line >> $log
done

# for dir in ${diretorio[@]}
# do
#    echo "Diretorio $dir"
#    #escolha qual variavel abaixo a usar no nome do arquivo compactado
#    dirname=${dir##*/} # pega somente o nome do diretorio
#    pathdirname=`echo $dir | sed 's/\//-/g'` # pega o path completo do diretorio e substitui a bassa pelo traço

#    tar -czvf $dir_bkp/$pathdirname-`date +%Y%m%d`.tar.gz --absolute-names $dir >> $log
# done

# remove backups older than 5 days
find $dir_bkp -mtime +5 -exec rm {} \;

# Run aws cli s3
# /usr/local/bin/aws s3 cp /etc/hosts s3://hsn.cliente01/ --profile backup
# /usr/local/bin/aws s3 sync /backup s3://BUCKET-BACKUP --delete

/usr/local/bin/aws s3 sync $dir_bkp s3://hsn.cliente01/ --profile backup

#/usr/local/bin/aws s3 -> Caminho da ferramente com o serviço
#sync $dir_bkp -> Diretorio a ser backupeado
#s3://hsn.cliente01/ -> Bucket 
#--profile backup -> Profile
# --delete -> Muito cuidado com este comando pois caso o usuario apague algum arquivo local, ao executar com esta opção tambem será apagado no bucket
echo "Fim do Backup ! "-`date +%d/%m/%Y-%H:%M:%S` >> $log;
echo " " >> $log;

echo -e "Enviando e-mail com os logs"
/usr/local/bin/aws ses send-email --from $mailOrigem --to $mailDestino --subject "Backup Dados" --text "`tail -n20 $log`" --profile backup
echo -e "Backup Terminado"-`date +%d/%m/%Y-%H:%M:%S`


# /usr/local/bin/aws ses send-email -> Caminho da ferramenta com o serviço
# --from $mailOrigem  -> Email de Origem --- verificado no serviço SES
# --to $mailDestino  -> Email de Destino --- verificado no serviço SES
# --subject "Backup Dados" --text "`tail -n20 $log`" -> Corpo da mensagem --- o comando tail envia como saida as ultimas linhas que com o -n20, envias as ultimas 20 linhas do arquivo
# --profile backup -> profile do usuario criado no IAM e configurado pelo comando $aws configure --profile backup e o mesmo fica salvo em ~/.aws/credentials