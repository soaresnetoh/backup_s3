#Script de Backup no S3
#Desenvolvido Hernani Soares
#https://www.linkedin.com/in/soaresnetoh/
#07/12/2020
Conteudo necessário para o processo de Backup em Nuvem utilizando o CLI, S3, SES e outros serviços da AWS

Para que se possa realizar o Backup em Nuvem na nuvem da AWS são necessários alguns passo que vou colocar aqui porem não serão amplamente visualizados por o objetivo aqui é o desenvolvimento de um script bash para backup em Linux.... 

Em algum monento a frente, este script será disponibilizado tambem para Windows.

1) Criar um BUCKET no S3 de nome Unico. Ex: minhaempresa.cliente01 -> guardar este nome
2) Criar uma Política no IAM onde deverá dar acesso ao S3 com direitos de Listar, Leitura, Escrita (OBS: Cuidado com o acesso a deletar arquivos). Coloque tambem acesso ao SES - SendEmail para que possa ser enviado um email com o log do Backup (nao esqueça de autenticar os emails de envio e de recebimento). Ex: Politica_Minhaempresa.Cliente01-S3
3) Criar Usuario no IAM, anexar esta política. Este usuario será configurado no aws cli para que os comandos funcionem. Não esqueça de ter guardado o Access Key e Security Key deste usuario.
4) No terminado do Linux executa: $sudo apt-get update && sudo apt-get install awscli -y
5) Apos instalado, configurar o CLI: $aws configure
6) crie o script (ou copie daqui) e atualiza as variaveis.